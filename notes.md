2.1 Для того, щоб додати нове домашнє завдання в новий репозиторій є 2 шляхи. 

Перший спосіб: Створюємо новий репозиторій на сайті Gitlab.com, клонуємо його на свій компʼютер за допомогою команди git clone ,  створюємо файли з домашнім завданням (можливо створювати файли через команду touch і назва і формат файлк) і комітимо його (git add ., git commit -m) "назва комміту", git push -u origin main/master( для найпершого пушу, надалі можна просто git push origin main/master).

Другий спосіб: Створюємо файли з домашнім завданням в папці , а тоді вже створюємо репозиторій. 
коли перші 2 кроки виконані:
1. переходимо в папку з домашнім завданням , яку ми б хотіли додати в репозторій через команду cd existing_folder (де existing_folder це назва нашої папки )
2. вказуємо що ми будемо додовати нашу папку саме в репозиторій через команду git init --initial-branch=main 
3. Вказуємо з яким саме репозиторієм ми хочепо зв'язати наші зміни ( папку) через команду git remote add origin https://gitlab.com/Balalex/hw_1_havrylchenko_git.git ( де посилання HTTPS формату на наш репозиорій)
4. Створюмо коміт з назвою в лапках:
git add .
git commit -m "Initial commit"
5.  пушимо наші зміни в репозиторій git push --set-upstream origin main